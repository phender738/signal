﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Dtos;
using WebApi.Models;

namespace WebApi.Repo
{
    public static class Extensions
    {
        public static CustomerDto AsDto(this Customer customer )
        {
            return new CustomerDto 
            {
                Id = customer.Id,
                Firstname = customer.Firstname,
                Lastname = customer.Lastname
            };
        }
    }
}
