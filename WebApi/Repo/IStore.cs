﻿using System;
using System.Collections.Generic;
using WebApi.Models;

namespace WebApi.Repo
{
    public interface IStore
    {
        Customer GetCustomer(Guid id);
        IEnumerable<Customer> GetCustomers();
        void CreateCustomer(Customer customer);
        void UpdateCustomer(Customer customer);
    }
}