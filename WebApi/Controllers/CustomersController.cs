﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Dtos;
using WebApi.Models;
using WebApi.Repo;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IStore Storage;

        public CustomersController(IStore storage)
        {
            Storage = storage;
        }


        //GET /Customers
        [HttpGet]
        public IEnumerable<CustomerDto> GetCustomers()
        {
            return Storage.GetCustomers().Select(c => c.AsDto());
        }

        //GET /Customers/{id}
        [HttpGet("{id}")]
        public ActionResult<CustomerDto> GetCustomer(Guid id)
        {
            var ret = Storage.GetCustomer(id);
            if (ret is null)
            {
                return NotFound();
            }
            return ret.AsDto();
        }


        //POST /Customers
        [HttpPost]
        public ActionResult<CustomerDto> CreateCustomer(CreateCustomerDto customerDto)
        {
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                Firstname = customerDto.Firstname,
                Lastname = customerDto.Lastname
            };
            Storage.CreateCustomer(customer);
            return CreatedAtAction(nameof(GetCustomer), new { id = customer.Id }, customer.AsDto());

        }


        //PUT /Customers/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateCustomer(Guid id,UpdateCustomerDto customerDto)
        {
            var rec = Storage.GetCustomer(id);
            if(rec is null)
            {
                return NotFound();
            }
            
            
            Storage.UpdateCustomer(
            new Customer
            {
                Id = id,
                Firstname = customerDto.Firstname,
                Lastname = customerDto.Lastname
            }
            );
            return NoContent();
        }

    }
}
