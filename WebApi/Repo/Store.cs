﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Repo
{
    public class Store : IStore
    {
        public List<Customer> Customers = new()
        {
            new Customer { Id = Guid.NewGuid(), Firstname = "Paul", Lastname = "Henderson" },
            new Customer { Id = Guid.NewGuid(), Firstname = "Fred", Lastname = "Bloggs" },
            new Customer { Id = Guid.NewGuid(), Firstname = "John", Lastname = "Smith" },
            new Customer { Id = Guid.NewGuid(), Firstname = "Boris", Lastname = "Johnson" },
        };

        public IEnumerable<Customer> GetCustomers()
        {
            return Customers;
        }

        public Customer GetCustomer(Guid id)
        {
            return Customers.Where(c => c.Id == id).FirstOrDefault();//.Where(c => c.Guid == id);

        }

        public void CreateCustomer(Customer customer)
        {
            Customers.Add(customer);
        }

        public void UpdateCustomer(Customer customer)
        {
            var index = Customers.FindIndex(existing => existing.Id == customer.Id);
            Customers[index] = customer;
        }
    }
}
